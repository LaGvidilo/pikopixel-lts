//
//  AppDelegate.h
//  pikopixel-lts
//
//  Created by Rick Sanchez on 23/10/2019.
//  Copyright © 2019 Rick Sanchez. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

